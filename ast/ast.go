// Functions to parse the tokens and build the AST.
package ast

import (
	"errors"
	"fmt"
	"gocompiler/token"
)

var EOP = errors.New("EOP") // End Of Parser
var UnexpectedEOP = errors.New("UnexpectedEOP")

// Expression types

// Common expression interface
type Expr interface{ exprDummy() }

// The prototype for a function, for example: `def foo(a, b) (a + b);` has a
// prototype of `{Name: "foo", Params: ["a", "b"]}`.
type Prototype struct {
	Name   string
	Params []string
}

type Number float64
type Variable string
type Extern struct{ Prototype Prototype }
type Return struct{ Expr Expr }
type Definition struct {
	Prototype Prototype
	Expr      Expr
}
type Binary struct {
	Expr1 Expr
	Op    token.BinaryOpToken
	Expr2 Expr
}
type Call struct {
	Function string // Could be token.Identifier?
	Args     []Expr
}
type IfElse struct {
	Cond Expr
	Then Expr
	Else Expr
}

func (e Number) exprDummy()     {}
func (e Variable) exprDummy()   {}
func (e Extern) exprDummy()     {}
func (e Return) exprDummy()     {}
func (e Definition) exprDummy() {}
func (e Binary) exprDummy()     {}
func (e Call) exprDummy()       {}
func (e IfElse) exprDummy()     {}

type Parser struct {
	Tokens      []token.Token
	Index       int
}

// Create a new zero-initialised parser using the given token set.
func NewParser(toks []token.Token) *Parser {
	return &Parser{toks, 0}
}

// Get the token at the current position. Panics with out of bounds error
// if index > len(p.tokens).
func (p *Parser) Current() token.Token { return p.Tokens[p.Index] }

// Panic and print current token and token index.
// TODO: Add line and column info if we add it to tokens
func (p *Parser) unexpectedToken() error {
	return fmt.Errorf("Unexpected token '%s' at index %d", p.Current(), p.Index)
}

// advance the parser one token. If `failOnEOP` is true and we are at the
// end of the parser, we panic.
func (p *Parser) advance(failOnEOP bool) error {
	if p.Index == len(p.Tokens)-1 {
		if failOnEOP {
			panic(UnexpectedEOP)
		}
		return EOP
	}
	p.Index++
	return nil
}

// Consume the current token if it is the StaticToken `tok`. If we reach
// the end of the tokens and `failOnEOP` is true, we panic.
func (p *Parser) ParseStaticToken(tok token.StaticToken, failOnEOP bool) {
	if p.Current() != tok {
		panic(p.unexpectedToken())
	}
	p.advance(failOnEOP)
}

// Parse a single identifier and return its value.
func (p *Parser) ParseIdentifier(failOnEOP bool) string {
	if iden, ok := p.Current().(token.Identifier); ok {
		p.advance(failOnEOP)
		return string(iden)
	}
	panic(p.unexpectedToken())
}

// Parse all tokens separated by token sep, until stop token is reached.
// Parse function is used to parse items, if it returns <nil>, the item will
// be skipped. multiSep determines whether multiple separators in a row is
// allowed. If not, an unexpectedToken error is thrown if two in a row are
// encountered.
func (p *Parser) parseSeparated(
	sep, stop token.Token,
	parseFn func() interface{},
	multiSep bool,
) (items []interface{}) {
	if p.Current() == stop {
		// Return immediately if there's nothing to be done
		goto Ret
	}

	for {
		if item := parseFn(); item != nil {
			items = append(items, item)
		}

		tok := p.Current()
		advanced := false
		for tok == sep {
			if advanced && !multiSep {
				panic(p.unexpectedToken())
			}
			p.advance(true)
			advanced = true
			tok = p.Current()
		}
		if tok == stop {
			break
		} else if !advanced {
			// No separators were found...
			panic(p.unexpectedToken())
		}
	}
Ret:
	p.advance(false)
	return
}

// Parse a block of expressions, where a block is a set of expressions within
// curly braces, separated by newlines.
func (p *Parser) ParseBlock() Expr {
	p.ParseStaticToken(token.LeftCurly, true)
	parseFn := func() interface{} { return p.ParseExpression() }
	items := p.parseSeparated(token.Newline, token.RightCurly, parseFn, true)

	var exprs []Expr
	for _, item := range items {
		exprs = append(exprs, item.(Expr))
	}
	// TODO: currently we only support single statement blocks. This can be
	// updated once we support multi-statement blocks and return statements
	return exprs[0]
}

// Parse comma separated "things". This is psuedo-generic function for
// parsing comma separated identifiers (for prototypes) or comma separated
// expressions (for function calls).
func (p *Parser) parseCommaSeparated(parseFn func() interface{}) (items []interface{}) {
	p.ParseStaticToken(token.LeftParen, true)
	return p.parseSeparated(token.Comma, token.RightParen, parseFn, false)
}

// Parse a set of comma separated Identifiers (and enclosing parentheses)
func (p *Parser) ParseIdentifiers() (strs []string) {
	parseFn := func() interface{} { return p.ParseIdentifier(true) }
	items := p.parseCommaSeparated(parseFn)
	for _, item := range items {
		strs = append(strs, item.(string))
	}
	return
}

// Parse a set of comma separated Expressions (and enclosing parentheses).
// NB this is distinct from ParseBlock in that expressions are enclosed
// in regular parentheses and comma separated.
func (p *Parser) ParseExpressions() (exprs []Expr) {
	parseFn := func() interface{} { return p.ParseExpression() }
	items := p.parseCommaSeparated(parseFn)
	for _, item := range items {
		exprs = append(exprs, item.(Expr))
	}
	return
}

func (p *Parser) ParsePrototype() Prototype {
	str := p.ParseIdentifier(true)
	params := p.ParseIdentifiers()
	return Prototype{Name: str, Params: params}
}

func (p *Parser) ParseExtern() Extern {
	p.ParseStaticToken(token.Extern, true)
	proto := p.ParsePrototype()
	return Extern{proto}
}

// Parse a return statement
func (p *Parser) ParseReturn() Return {
	p.ParseStaticToken(token.Return, true)
	expr := p.ParseExpression()
	switch expr.(type) {
	case Call, Binary, Variable, Number:
		// no-op, this is a legitimate return expression
	default:
		panic(p.unexpectedToken())
	}
	return Return{expr}
}

func (p *Parser) ParseDefinition() Definition {
	p.ParseStaticToken(token.Def, true)
	proto := p.ParsePrototype()
	exprs := p.ParseBlock()
	return Definition{proto, exprs}
}

// Parse an if-statement
func (p *Parser) ParseIfElse() IfElse {
	p.ParseStaticToken(token.If, true)

	// Parse left paren, condition, and right paren
	p.ParseStaticToken(token.LeftParen, true)
	Cond := p.ParseExpression()
	p.ParseStaticToken(token.RightParen, true)

	// Parse if block
	Then := p.ParseBlock()

	// The else block is optional
	var Else Expr
	if p.Current() == token.Else {
		p.ParseStaticToken(token.Else, true)
		// TODO: allow for else if
		Else = p.ParseBlock()
	} else {
		Else = Expr(nil)
	}

	return IfElse{Cond, Then, Else}
}

func (p *Parser) ParseExpression() (expr Expr) {
	switch tok := p.Current().(type) {
	case token.StaticToken:
		switch tok {
		case token.LeftParen:
			// Parse a nested expression
			p.advance(true)
			expr = p.ParseExpression()
			p.ParseStaticToken(token.RightParen, false)
		case token.If:
			expr = p.ParseIfElse()
		case token.Return:
			expr = p.ParseReturn()
		case token.Newline:
			return nil
		case token.Extern:
			expr = p.ParseExtern()
		case token.Def:
			expr = p.ParseDefinition()
		}
	case token.Number:
		// Create a number
		p.advance(false)
		expr = Number(float64(tok))
	case token.Identifier:
		if p.advance(false) == EOP || p.Current() != token.LeftParen {
			// If we've reached the end of parser or the next token was not a left
			// parenthesis, it's a variable
			expr = Variable(string(tok))
		} else {
			// Otherwise it's a function call
			args := p.ParseExpressions()
			expr = Call{string(tok), args}
		}
	default:
		panic(p.unexpectedToken())
	}

	// Now we're done with the first expression, if the current token is a
	// BinaryOpToken, then we parse that and the next expression, then return
	// a Binary expression
	switch tok := p.Current().(type) {
	case token.BinaryOpToken:
		p.advance(true)
		secondExpr := p.ParseExpression()

		// TODO: Currently unsupported, create an assign expression
		// (see https://llvm.org/docs/tutorial/OCamlLangImpl7.html), this will
		// replace the PHI node mechanics in IfElse
		if tok == token.Assign {
			panic("Assignment currently unsupported")
			if _, ok := expr.(Variable); !ok {
				// Cannot assign to a non-variable type
				panic(fmt.Errorf("Cannot assign to expression of type %T", expr))
			}
		}

		return Binary{expr, tok, secondExpr}
	}
	return expr
}

func (p *Parser) ParseAll() (externs []Prototype, defs []Definition, exprs []Expr) {
	for ; p.Index < len(p.Tokens); p.Index++ {
		expr := p.ParseExpression()

		switch expr := expr.(type) {
		case Extern:
			externs = append(externs, expr.Prototype)
		case Definition:
			defs = append(defs, expr)
		case Expr:
			exprs = append(exprs, expr)
		case nil:
			// no-op
		default:
			panic(fmt.Errorf("Bad expression: %#v", expr))
		}
	}
	return
}
