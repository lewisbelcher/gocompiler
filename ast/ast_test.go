// TODO: this can probably consolidated into a single test pattern, testing
// an input and expected output using a parser function (required a common
// output type? List of expressions?)

package ast

import (
	"gocompiler/token"
	"testing"
)

var p *Parser

func stringSlicesEqual(s1, s2 []string) (match bool) {
	if len(s1) != len(s2) {
		return false
	}
	match = true
	for i, str := range s1 {
		if str != s2[i] {
			return false
		}
	}
	return
}

func exprsEqual(exprs1, exprs2 []Expr) bool {
	if len(exprs1) != len(exprs2) {
		return false
	}
	for i, expr := range exprs1 {
		if exprs2[i] != expr {
			return false
		}
	}
	return true
}

func TestParseStaticTokenBasic(t *testing.T) {
	// Test a basic case
	p = NewParser([]token.Token{token.Semicolon})
	p.ParseStaticToken(token.Semicolon, false)
}

func TestParseStaticTokenUnexpectedToken(t *testing.T) {
	// Test panics with UnexpectedToken
	p = NewParser([]token.Token{token.Semicolon})
	defer func() {
		if p := recover(); p == nil {
			t.Error("ParseStaticToken did not panic for unexpected token")
		}
	}()
	p.ParseStaticToken(token.RightParen, false)
}

func TestParseStaticTokenFailOnEOP(t *testing.T) {
	// Test panics with UnexpectedEOP if failOnEOP is true
	p = NewParser([]token.Token{token.Semicolon})
	defer func() {
		if p := recover(); p != UnexpectedEOP {
			t.Error("ParseStaticToken did not panic for UnexpectedEOP")
		}
	}()
	p.ParseStaticToken(token.Semicolon, true)
}

func TestParseIdentifier(t *testing.T) {
	// Test basic case
	p = NewParser([]token.Token{token.Identifier("hi")})
	str := p.ParseIdentifier(false)
	if str != "hi" {
		t.Error("ParseIdentifier returned an incorrect string")
	}

	// Test panics when failOnEOP is true
	p = NewParser([]token.Token{token.Identifier("hi")})
	defer func() {
		if e := recover(); e != UnexpectedEOP {
			t.Error("ParseIdentifier did not panic for UnexpectedEOP")
		}
	}()
	str = p.ParseIdentifier(true)
}

func testIdentifiers(t *testing.T, p *Parser, truth []string) {
	strs := p.ParseIdentifiers()

	// check that the returned slice of identifier strings was correct
	if !stringSlicesEqual(strs, truth) {
		t.Error("ParseIdentifiers returned an unexpected array:", strs)
	}
}

func TestParseIdentifiers(t *testing.T) {
	// Check with a valid list of tokens
	p = NewParser([]token.Token{
		token.LeftParen,
		token.Identifier("a"),
		token.Comma,
		token.Identifier("b"),
		token.RightParen,
	})
	testIdentifiers(t, p, []string{"a", "b"})

	// Check with a trailing comma in arguments
	p = NewParser([]token.Token{
		token.LeftParen,
		token.Identifier("a"),
		token.Comma,
		token.Identifier("b"),
		token.Comma,
		token.RightParen,
	})
	testIdentifiers(t, p, []string{"a", "b"})

	// Check returns empty slice when no identifiers present
	p = NewParser([]token.Token{
		token.LeftParen,
		token.RightParen,
	})
	testIdentifiers(t, p, []string{})

	// Check panics with UnexpectedEOP
	p = NewParser([]token.Token{
		token.LeftParen,
		token.Identifier("a"),
		token.Comma,
	})
	defer func() {
		if p := recover(); p == nil {
			t.Error("parseCommaSeparated failed to panic")
		}
	}()
	p.ParseIdentifiers()
}

func TestParseBlock(t *testing.T) {
	// NB we currently only support single statement blocks!
	p = NewParser([]token.Token{
		token.LeftCurly,
		token.Identifier("a"),
		token.Times,
		token.Number(2.1),
		token.Newline,
		token.Newline, // NB should work with two newlines
		token.Identifier("b"),
		token.Times,
		token.Number(2.2),
		token.Newline,
		token.RightCurly,
	})
	truth := Binary{Variable("a"), token.Times, Number(2.1)}
	if expr := p.ParseBlock(); expr != truth {
		t.Errorf("Incorrect expressions in ParseBlock, expected %+v, got %+v", truth, expr)
	}
}

func TestParsePrototype(t *testing.T) {
	p = NewParser([]token.Token{
		token.Identifier("hello"),
		token.LeftParen,
		token.Identifier("a"),
		token.RightParen,
	})
	proto := p.ParsePrototype()
	if !stringSlicesEqual(proto.Params, []string{"a"}) {
		t.Error("ParsePrototype returned unexpected params:", proto.Params)
	}
}

func TestParseExtern(t *testing.T) {
	p = NewParser([]token.Token{
		token.Extern,
		token.Identifier("hello"),
		token.LeftParen,
		token.Identifier("a"),
		token.RightParen,
		token.Semicolon,
	})
	response := p.ParseExtern().Prototype.Params
	truth := []string{"a"}
	if !stringSlicesEqual(response, truth) {
		t.Error("ParsePrototype returned unexpected params:", response)
	}
}

func TestParseReturn(t *testing.T) {
	// Test basic case
	p = NewParser([]token.Token{
		token.Return,
		token.Identifier("a"),
	})
	truth := Return{Variable("a")}
	response := p.ParseReturn()
	if truth != response {
		t.Error("ParseReturn failed!")
	}
}

func TestParseVariable(t *testing.T) {
	p = NewParser([]token.Token{
		token.Identifier("var"),
	})
	expr := p.ParseExpression()
	if varExpr, ok := expr.(Variable); !ok {
		t.Error("Failed to parse Variable")
	} else if varExpr != Variable("var") {
		t.Errorf("Incorrect Variable: %v", varExpr)
	}
}

func TestParseIfElse(t *testing.T) {
	// Test IfElse expression
	p = NewParser([]token.Token{
		token.If,
		token.LeftParen,
		token.Identifier("hello"),
		token.RightParen,
		token.LeftCurly,
		token.Number(6),
		token.RightCurly,
		token.Else,
		token.LeftCurly,
		token.Number(9),
		token.RightCurly,
	})
	expr := p.ParseExpression()
	if ifElse, ok := expr.(IfElse); !ok {
		t.Error("Failed to parse IfElse")
	} else if expr, ok := ifElse.Cond.(Variable); !ok || expr != Variable("hello") {
		t.Errorf("Incorrect Cond in ifElse: %v", expr)
	} else if expr, ok := ifElse.Then.(Number); !ok {
		t.Errorf("Incorrect Then in ifElse: %v", expr)
	} else if expr, ok := ifElse.Else.(Number); !ok {
		t.Errorf("Incorrect Else in ifElse: %v", expr)
	}
}

func TestParseCall(t *testing.T) {
	p = NewParser([]token.Token{
		token.Identifier("functionName"),
		token.LeftParen,
		token.Identifier("var"),
		token.Comma,
		token.Number(4.4),
		token.RightParen,
	})
	expr := p.ParseExpression()
	if callExpr, ok := expr.(Call); !ok {
		t.Error("Failed to parse Call")
	} else if callExpr.Function != "functionName" {
		t.Errorf("Incorrect Call: %v", callExpr)
	} else if e, _ := callExpr.Args[0].(Variable); e != Variable("var") {
		t.Errorf("Incorrect Expr in callExpr.Args[0]: %v", callExpr.Args[0])
	} else if e, _ := callExpr.Args[1].(Number); e != Number(4.4) {
		t.Errorf("Incorrect Expr in callExpr.Args[1]: %v", callExpr.Args[1])
	}
}

func TestParseExpression(t *testing.T) {
	// Test parse number
	p = NewParser([]token.Token{
		token.Number(4.4),
	})
	expr := p.ParseExpression()
	if numExpr, ok := expr.(Number); !ok {
		t.Error("Failed to parse Number")
	} else if numExpr != Number(4.4) {
		t.Errorf("Incorrect Number: %v", numExpr)
	}

	// Test expression nested in paretheses
	p = NewParser([]token.Token{
		token.LeftParen,
		token.Identifier("var"),
		token.RightParen,
	})
	expr = p.ParseExpression()
	if varExpr, ok := expr.(Variable); !ok {
		t.Error("Failed to parse Variable")
	} else if varExpr != Variable("var") {
		t.Errorf("Incorrect Variable: %v", varExpr)
	}
}
