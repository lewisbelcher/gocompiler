package main

import (
	"io/ioutil"
	"llvm/bindings/go/llvm"
)

func main() {
	mod := llvm.NewModule("main")

	// Declare the puts function which will print our string (the actual
	// function is an external one)
	putsType := llvm.FunctionType(
		llvm.Int32Type(),
		[]llvm.Type{llvm.PointerType(llvm.Int8Type(), 0)},
		false,
	)
	puts := llvm.AddFunction(mod, "puts", putsType)

	// Create a builder
	builder := llvm.NewBuilder()

	// Create a main function
	main := llvm.FunctionType(llvm.VoidType(), []llvm.Type{}, false)
	function := llvm.AddFunction(mod, "main", main)

	// Add a block to this function
	block := llvm.AddBasicBlock(function, "entry")

	// NB functions can be retrieved using:
	// mod.NamedFunction("main")

	// Tell the builder where to insert instructions
	builder.SetInsertPointAtEnd(block)

	// Create a global string containing hello world
	hello := builder.CreateGlobalStringPtr("Hello, world!\n", "hello-world")

	// Create a call to puts, the return variable we will just call 0
	builder.CreateCall(puts, []llvm.Value{hello}, "0")

	// The module will return void (return code 192)
	builder.CreateRetVoid()

	// Verify the module
	if err := llvm.VerifyModule(mod, llvm.ReturnStatusAction); err != nil {
		panic(err)
	}

	// Output module contents as an .ll file
	output := []byte(mod.String())
	if err := ioutil.WriteFile("hworld.ll", output, 0644); err != nil {
		panic(err)
	}
}
