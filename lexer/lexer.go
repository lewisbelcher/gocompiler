// The lexer package implements relevant structs and methods for source code
// lexical analysis.
package lexer

import (
	"bufio"
	"fmt"
	"gocompiler/token"
	"io"
	"strconv"
)

func IsWhitespace(c rune) bool {
	switch c {
	case ' ', '\t', '\v':
		return true
	}
	return false
}

// Represents a position in the source code
type Position struct {
	line int
	col  int
}

// A placeholder for a token and its underlying position
type Item struct {
	Pos Position
	Tok token.Token
}

type Lexer struct {
	line   int // Line in reader
	col    int // Column on line
	reader *bufio.Reader
}

// Create a new lexer instance
func New(rd *bufio.Reader) *Lexer {
	return &Lexer{0, 0, rd}
}

// Read a single rune from l.reader. NB we only ever return nil or io.EOF
// for errors, for all other errors we panic immediately.
func (l *Lexer) ReadRune() (rune, error) {
	rn, _, err := l.reader.ReadRune()
	l.col += 1 // NB adjustment for newlines occurs in SkipWhitespace
	if err != nil && err != io.EOF {
		panic(fmt.Errorf("Error reading rune at line %d col %d: %v", l.line, l.col, err))
	}
	return rn, err
}

func (l *Lexer) UnreadRune() {
	if err := l.reader.UnreadRune(); err != nil {
		panic(err)
	}
	l.col -= 1
}

// Read a single complete token, whether that be a static token, number,
// identifier, etc.
func (l *Lexer) ReadToken() (tok token.Token, err error) {
	cur, err := l.ReadRune()

	if IsWhitespace(cur) {
		// If we ever reach here, it suggests an internal bug
		panic(fmt.Errorf("Unexpected whitespace"))
	}

	// Check for single token
	if tok, ok := token.StaticTokenMap[string(cur)]; ok {
		if tok == token.Comment {
			// For a comment, we swallow all text up to and including \n, then
			// kindly return a newline, even if it was missing at EOF
			_, err = l.reader.ReadString('\n')
			tok = token.Newline
		}
		if tok == token.Newline {
			l.col = 0
			l.line += 1
		} else if tok == token.Assign {
			if next, err := l.reader.Peek(1); err == nil && next[0] == '=' {
				_, err = l.ReadRune()
				return token.Equals, err
			}
		}
		return tok, err
	}

	// Collect runes
	chars := []rune{}
	for err != io.EOF {
		chars = append(chars, cur)
		cur, err = l.ReadRune()
		if token.StaticTokenMap[string(cur)] != nil || IsWhitespace(cur) {
			// NB although we would not need to unread a rune for whitespace, we do
			// so because it's easier to let SkipWhitespace handle the position logic
			l.UnreadRune()
			break
		}
	}
	str := string(chars)

	// Check for long token
	if tok, ok := token.StaticTokenMap[str]; ok {
		return tok, err
	}

	// Check for numeric
	if num, f64Err := strconv.ParseFloat(str, 64); f64Err == nil {
		return token.Number(num), err
	}

	// return identifier
	return token.Identifier(str), err
}

// Skip all whitespace
func (l *Lexer) SkipWhitespace() error {
	for {
		cur, err := l.ReadRune()

		switch cur {
		case ' ', '\t', '\v':
			// No-op
		case 0: // EOF
			return err
		default:
			l.UnreadRune()
			return err
		}
	}
}

// Lex over the entire reader l.reader and produce an array of tokens.
func (l *Lexer) Lex() (toks []token.Token) {
	var tok token.Token

	for err := l.SkipWhitespace(); err != io.EOF; {
		tok, err = l.ReadToken()
		if tok != nil {
			toks = append(toks, tok)
		}
		if err != nil && err != io.EOF {
			panic(err)
		}
		err = l.SkipWhitespace()
	}
	return
}
