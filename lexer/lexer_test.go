package lexer

import (
	"bufio"
	"gocompiler/token"
	"io"
	"strings"
	"testing"
)

var l *Lexer // Used in almost all tests

func lexerFromString(s string) *Lexer {
	b := bufio.NewReader(strings.NewReader(s))
	return New(b)
}

func TestIsWhitespace(t *testing.T) {
	// Check characters not recognised as whitespace
	for _, c := range "abcde(=+/" {
		if IsWhitespace(c) {
			t.Errorf("Character '%c' wrongly recognised as whitespace", c)
		}
	}

	// Check whitespace correctly recognised
	for _, ws := range " \t\v" {
		if !IsWhitespace(ws) {
			t.Errorf("Rune '%c' is not recognised as whitespace", ws)
		}
	}
}

func TestReadRune(t *testing.T) {
	s := "hello"
	l = lexerFromString(s)

	for i, truth := range s {
		rn, err := l.ReadRune()
		if rn != truth {
			t.Errorf("Error reading rune '%c', got '%c'", truth, rn)
		}
		if l.col != i+1 {
			t.Errorf("Incorrect column number, expected %d, got %d", i, l.col)
		}
		if err != nil {
			t.Errorf("Unexpected error in ReadRune: %v", err)
		}
	}
	if _, err := l.ReadRune(); err != io.EOF {
		t.Errorf("Expected io.EOF error, got: %v", err)
	}
}

func TestUnreadRune(t *testing.T) {
	s := "hello"
	l = lexerFromString(s)

	l.ReadRune()
	l.UnreadRune()
	if l.col != 0 {
		t.Errorf("Expected l.col to be at 4, got %d", l.col)
	}
}

func TestReadToken(t *testing.T) {
	// Check ReadToken correctly returns an identifier
	s := "Hello"
	l = lexerFromString(s)
	tok, _ := l.ReadToken()
	if tok, ok := tok.(token.Identifier); !ok {
		t.Errorf("tok should be token.Identifier, got: %v", tok)
	} else if tok != token.Identifier(s) {
		t.Errorf("tok should be %#v, got %#v", s, tok)
	}

	// Check ReadToken correctly returns a token.Number token
	l = lexerFromString("3.14")
	tok, _ = l.ReadToken()
	if num, ok := tok.(token.Number); !ok {
		t.Error("tok should be a number")
	} else if num != token.Number(3.14) {
		t.Error("num should be 3.14")
	}

	// Check all StaticTokens parsed correctly
	for str, expected := range token.StaticTokenMap {
		l = lexerFromString(str)
		tok, _ = l.ReadToken()
		if str == "#" {
			// NB in our simple parser we ignore all comments and return a newline
			expected = token.Newline
		}
		if stok, ok := tok.(token.StaticToken); !ok || stok != expected {
			t.Errorf("Error parsing StaticToken, expected %v, got %v", expected, tok)
		}
	}

	// Check that at whitepace at current position raises an error
	l = lexerFromString(" ")
	defer func() {
		if p := recover(); p == nil {
			t.Error("ReadToken should have failed on unexpected whitespace")
		}
	}()
	l.ReadToken()
}

func TestSkipWhitespace(t *testing.T) {
	l = lexerFromString(" \t\v  hello")
	_ = l.SkipWhitespace()
	if l.col != 5 {
		t.Errorf("Position after SkipWhitespace should be 5, got %d", l.col)
	}

	// Test returns io.EOF
	l = lexerFromString(" \t\v  ")
	err := l.SkipWhitespace()
	if err != io.EOF {
		t.Errorf("SkipWhitespace should return io.EOF, got %v", err)
	}
}

func TestLex(t *testing.T) {
	// Check the lexer finds all of the correct tokens
	l = lexerFromString(`
	def func(x) {
		return x * 100.31
	};
	`)

	truth := []token.Token{
		token.Newline,
		token.Def,
		token.Identifier("func"),
		token.LeftParen,
		token.Identifier("x"),
		token.RightParen,
		token.LeftCurly,
		token.Newline,
		token.Return,
		token.Identifier("x"),
		token.Times,
		token.Number(100.31),
		token.Newline,
		token.RightCurly,
		token.Semicolon,
		token.Newline,
	}
	toks := l.Lex()
	for i, tok := range toks {
		if tok != truth[i] {
			t.Errorf("Token mismatch: expected %v found %v", truth[i], tok)
		}
	}
	if l.col != 2 {
		t.Errorf("Expected final position to be col 2, got %d", l.col)
	}
	if l.line != 4 {
		t.Errorf("Expected final position to be line 4, got %d", l.line)
	}
}
