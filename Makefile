
SOURCEDIRS = ast lexer token

.PHONY: fmt
fmt:
	@r=0; for d in $(SOURCEDIRS) ; do \
	  cd $$d ; \
	  go fmt *go ; \
	  cd .. ; \
	done

.PHONY: test
test:
	@rv=0; for d in $(SOURCEDIRS) ; do \
	  cd $$d ; \
	  go test -v ; \
	  let rv+=$$? ; \
	  cd .. ; \
	done ; exit $$rv

gocompiler: main.go ast/ast.go lexer/lexer.go token/token.go generator/generator.go
	@go build -ldflags="-s -w" -o $@ $<
