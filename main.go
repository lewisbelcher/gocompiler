
package main

import (
	"bufio"
	"flag"
	"fmt"
	"gocompiler/ast"
	"gocompiler/lexer"
	"gocompiler/generator"
	"gocompiler/token"
	"os"
	"text/tabwriter"
)

var verbose = flag.Bool("v", false, "make output more verbose")
var output = flag.String("o", "", "output file, default: stdout")

func printTokens(tokens []token.Token) {
	println("Tokens:")
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', 0)
	for _, tok := range tokens {
		fmt.Fprintf(w, "%T\t%v\n", tok, tok)
	}
	w.Flush()
	println()
}


func main() {
	flag.Parse()

	if flag.NArg() != 1 {
		fmt.Printf("Usage:  %s [options] <source>\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(2)
	}

	// Open input file
	f, err := os.Open(flag.Arg(0))
	if err != nil { panic(err) }
	defer f.Close()

	// Open output file
	var fo *os.File
	if *output == "" {
		fo = os.Stdout
	} else {
		fo, err = os.Create(*output)
		if err != nil { panic(err) }
		defer fo.Close()
	}

	b := bufio.NewReader(f)
	tokens := lexer.New(b).Lex()

	if *verbose { printTokens(tokens) }

	parser := ast.NewParser(tokens)
	externs, defs, exprs := parser.ParseAll()

	gen := generator.New()

	// Emit externs
	for _, extern := range externs {
		gen.EmitPrototype(extern)
	}

	// Emit definitions
	for _, def := range defs {
		gen.EmitDefinition(def)
	}

	// Emit expressions within main
	gen.EmitMain(exprs)

	gen.WriteExecutable(fo)
}
