package generator

import (
	"gocompiler/ast"
	"gocompiler/token"
	"strings"
	"testing"
)

// It's a little difficult to test the code generatation since we cannot inspect
// the `llvm.Value` objects directly. We can instead check the resultant module
// string however (`llvm.Module.String()`).

var source = strings.NewReader("def hello() { return 6 }")
var g = New(source)
var funcName = "testFunction"

func TestEmitPrototype(t *testing.T) {
	proto := ast.Prototype{funcName, []string{"1", "2"}}
	g.EmitPrototype(proto)

	if g.module.NamedFunction(proto.Name).IsNil() {
		t.Errorf("Expected function %s to exist in module", proto.Name)
	}

	truth := "hello"
	output := g.module.String()
	if output != truth {
		t.Errorf("Expected contents of module differ:\nGot:%s\nExpected:", output, truth)
	}
}

func TestEmitExpression(t *testing.T) {
	expr := ast.ExprNumber{50.0}
	g.EmitExpression(expr)
}

func TestEmitCall(t *testing.T) {
	// Valid
	args := []ast.Expr{ast.ExprNumber{9}, ast.ExprNumber{9}}
	g.EmitCall(ast.ExprCall{funcName, args})
}

func TestEmitCallUnknownFunction(t *testing.T) {
	// Unknown function
	defer func() {
		if e := recover(); e == nil {
			t.Error("Expected panic with unknown function")
		}
	}()
	g.EmitCall(ast.ExprCall{"noFunction", args})
}

func TestEmitCallArityError(t *testing.T) {
	// Arity error
	defer func() {
		if e := recover(); e == nil {
			t.Error("Expected panic with arity error")
		}
	}()
	g.EmitCall(ast.ExprCall{funcName, args})
}

func TestEmitBinary(t *testing.T) {
	testOps := []token.BinaryOpToken{
		token.Plus,
		token.Minus,
		token.Times,
		token.Divide,
		token.Mod,
		token.Equals,
	}
	for _, op := range testOps {
		g.EmitBinary(ast.ExprBinary{ast.ExprNumber{5.0}, op, ast.ExprNumber{3.0}})
	}
}
