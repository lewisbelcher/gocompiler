// Generate LLVM IR from a Kaleidoscope AST.
package generator

import (
	"fmt"
	"gocompiler/ast"
	"gocompiler/token"
	"os"
	"llvm/bindings/go/llvm"
)

var ZERO = llvm.ConstFloat(llvm.DoubleType(), 0)

type IRGenerator struct {
	module     llvm.Module
	builder    llvm.Builder
	prototypes map[string]ast.Prototype
	variables  map[string]llvm.Value
}

func New() *IRGenerator {
	protos := make(map[string]ast.Prototype)
	vars := make(map[string]llvm.Value)
	// TODO: add print function by default
	return &IRGenerator{llvm.NewModule("main"), llvm.NewBuilder(), protos, vars}
}

func (g *IRGenerator) Output() string {
	return g.module.String()
}

func (g *IRGenerator) resetVariables() {
	g.variables = make(map[string]llvm.Value)
}

func createDoubles(n int) (doubles []llvm.Type) {
	for i := 0; i < n; i++ {
		doubles = append(doubles, llvm.DoubleType())
	}
	return doubles
}

func (g *IRGenerator) EmitPrototype(proto ast.Prototype) llvm.Value {
	// If the function is already defined within g.module, return it
	if function := g.module.NamedFunction(proto.Name); !function.IsNil() {
		return function
	}

	// Otherwise, create and add it to g.module
	g.prototypes[proto.Name] = proto
	argTypes := createDoubles(len(proto.Params))
	funcType := llvm.FunctionType(llvm.DoubleType(), argTypes, false)
	function := llvm.AddFunction(g.module, proto.Name, funcType)
	for i, param := range proto.Params {
		function.Param(i).SetName(param)
	}
	return function
}

func (g *IRGenerator) EmitIfElse(expr ast.IfElse) llvm.Value {
	// CreateFCmp(pred FloatPredicate, lhs, rhs Value, name string) (v Value)
	// Using FloatONE (== C.LLVMRealONE; true if ordered and not equal) to
	// compare the result of the expression, to a constant 0.0
	cmp := g.builder.CreateFCmp(llvm.FloatONE, ZERO, g.EmitExpression(expr.Cond), "cmp")

	function := g.module.LastFunction()
	thenBB := llvm.AddBasicBlock(function, "then")
	elseBB := llvm.AddBasicBlock(function, "else")
	mergeBB := llvm.AddBasicBlock(function, "merge")

	// Create conditional branch value
	g.builder.CreateCondBr(cmp, thenBB, elseBB)

	// Emit the expressions for 'then' in the 'then' block
	g.builder.SetInsertPointAtEnd(thenBB)
	thenVal := g.EmitExpression(expr.Then)
	g.builder.CreateBr(mergeBB)  // go to merge block

	// Emit the expressions for 'else' in the 'else' block
	g.builder.SetInsertPointAtEnd(elseBB)
	elseVal := g.EmitExpression(expr.Else)
	g.builder.CreateBr(mergeBB)  // go to merge block

	g.builder.SetInsertPointAtEnd(mergeBB)

	phi := g.builder.CreatePHI(llvm.DoubleType(), "ifElsePhi")
	phi.AddIncoming(
		[]llvm.Value{thenVal, elseVal},
		[]llvm.BasicBlock{thenBB, elseBB},
	)
	return phi
}

func (g *IRGenerator) EmitBinary(expr ast.Binary) (value llvm.Value) {
	lhs := g.EmitExpression(expr.Expr1)
	rhs := g.EmitExpression(expr.Expr2)
	switch expr.Op {
	case token.Plus:
		value = g.builder.CreateFAdd(lhs, rhs, "add")
	case token.Minus:
		value = g.builder.CreateFSub(lhs, rhs, "sub")
	case token.Times:
		value = g.builder.CreateFMul(lhs, rhs, "mul")
	case token.Divide:
		value = g.builder.CreateFDiv(lhs, rhs, "div")
	case token.Mod:
		value = g.builder.CreateFRem(lhs, rhs, "rem")
	case token.Equals:
		// Using FloatOEQ (true if ordered and equal)
		ui := g.builder.CreateFCmp(llvm.FloatOEQ, lhs, rhs, "cmp")
		value = g.builder.CreateUIToFP(ui, llvm.DoubleType(), "UIToFP")
	default:
		panic(fmt.Errorf("Unsupported op for binary expression: %+v", expr.Op))
	}
	return
}

func (g *IRGenerator) EmitCall(call ast.Call) llvm.Value {
	proto, ok := g.prototypes[call.Function]
	if !ok {
		panic(fmt.Errorf("Unknown function: '%s'", call.Function))
	}

	expected, got := len(proto.Params), len(call.Args)
	if expected != got {
		msg := "Arity error: function '%s': expected %d, got %d"
		panic(fmt.Errorf(msg, call.Function, expected, got))
	}
	function := g.EmitPrototype(proto)

	var callArgs []llvm.Value
	for _, expr := range call.Args {
		callArgs = append(callArgs, g.EmitExpression(expr))
	}
	// NB the builder will automatically increment the return name for us
	return g.builder.CreateCall(function, callArgs, "0")
}

func (g *IRGenerator) EmitVariable(expr ast.Variable) llvm.Value {
	variable, ok := g.variables[string(expr)]
	if !ok {
		panic(fmt.Errorf("Unknown variable: '%s'", expr))
	}
	return variable
}

func (g *IRGenerator) EmitDefinition(def ast.Definition) llvm.Value {
	function := g.EmitPrototype(def.Prototype)

	for i, param := range def.Prototype.Params {
		g.variables[param] = function.Param(i)
	}

	block := llvm.AddBasicBlock(function, "entry")
	g.builder.SetInsertPointAtEnd(block)
	expr := g.EmitExpression(def.Expr)

	// Create return of the expression value
	g.builder.CreateRet(expr)

	// Clear the variables map
	g.resetVariables()

	return function
}

func (g *IRGenerator) EmitReturn(expr ast.Return) llvm.Value {
	value := g.EmitExpression(expr.Expr)
	return g.builder.CreateRet(value)
}

// Emit a general expression. NB nested functions are currently not supported.
func (g *IRGenerator) EmitExpression(expr ast.Expr) (value llvm.Value) {
	switch expr := expr.(type) {
	case ast.Number:
		value = llvm.ConstFloat(llvm.DoubleType(), float64(expr))
	case ast.Call:
		value = g.EmitCall(expr)
	case ast.Variable:
		value = g.EmitVariable(expr)
	case ast.IfElse:
		value = g.EmitIfElse(expr)
	case ast.Binary:
		value = g.EmitBinary(expr)
	default:
		panic(fmt.Errorf("Unknown expression: %#v", expr))
	}
	return value
}

// Create a function 'printf' for printing a string to stdout
func (g *IRGenerator) EmitPrintf() llvm.Value {
	if f := g.module.NamedFunction("printf"); !f.IsNil() {
		return f
	}
	printfType := llvm.FunctionType(
		llvm.Int32Type(),
		[]llvm.Type{llvm.PointerType(llvm.Int8Type(), 0)},
		true,  // NB argument is variadic
	)
	return llvm.AddFunction(g.module, "printf", printfType)
}

func (g *IRGenerator) EmitMain(exprs []ast.Expr) {
	// Main takes no args and returns void
	main := llvm.FunctionType(llvm.VoidType(), []llvm.Type{}, false)
	function := llvm.AddFunction(g.module, "main", main)
	block := llvm.AddBasicBlock(function, "entry")

	g.builder.SetInsertPointAtEnd(block)
	formatString := g.builder.CreateGlobalStringPtr("%f\n", "formatString")
	printf := g.EmitPrintf()

	for _, expr := range exprs {
		val := g.EmitExpression(expr)
		g.builder.CreateCall(printf, []llvm.Value{formatString, val}, "printf")
	}

	g.builder.CreateRetVoid()
}

// Write the underlying module of an IRGenerator as an executable.
// See https://llvm.org/docs/tutorial/LangImpl08.html for details on
// emitting assembly code for a target machine.
// TODO: Translate into IR -> BitCode -> Assembly -> Object -> Executable
// WriteBitcodeToMemoryBuffer(g.module).Bytes()
func (g *IRGenerator) WriteExecutable(file *os.File) {
	// Initialise all targets for emitting object code
	llvm.InitializeNativeTarget()
	llvm.InitializeNativeAsmPrinter()
	llvm.InitializeAllAsmParsers()

	// ttriple := llvm.DefaultTargetTriple()
	// t, err := llvm.GetTargetFromTriple(ttriple)
	// if err != nil { panic(err) }

	// We'll be using the generic CPU and no additional features. Run
	// `llvm-as < /dev/null | llc -march=x86 -mattr=help` to see the available
	// CPUs and features for target architectures.
	// cpu, features := "generic", ""
	// level := llvm.CodeGenLevelDefault
	// reloc := llvm.RelocDefault
	// model := llvm.CodeModelDefault
	// tm := t.CreateTargetMachine(ttriple, cpu, features, level, reloc, model)

	pm := llvm.NewPassManager()
	pm.AddGlobalOptimizerPass()
	pm.AddVerifierPass()
	pm.Run(g.module)
	llvm.WriteBitcodeToFile(g.module, file)
}
