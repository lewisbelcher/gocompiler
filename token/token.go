// The token package implements all required tokens for the Kaleidoscope
// compiler.
package token

import (
	"errors"
	"fmt"
)

var UnexpectedToken = errors.New("UnexpectedToken")

// A Token represents any source code token.
type Token interface{ tokenDummy() }

// TODO: contain positional information? In the Go compiler, position
// is a separate type

// Define all tokens
type StandardToken string
type BinaryOpToken string
type Identifier string
type Number float64

// StandardToken, BinaryOpToken, Identifier and Number are all Tokens
func (x StandardToken) tokenDummy() {}
func (x BinaryOpToken) tokenDummy() {}
func (x Identifier) tokenDummy()    {}
func (x Number) tokenDummy()        {}

// A StaticToken represents a token whose value is known and unvarying.
// E.g. a left parenthesis '(' or a comma ','.
type StaticToken interface {
	Token
	staticDummy()
}

// StandardToken and BinaryOpToken are StaticTokens
func (x StandardToken) staticDummy() {}
func (x BinaryOpToken) staticDummy() {}

// Define all StaticTokens
const (
	LeftParen  = StandardToken("(")
	RightParen = StandardToken(")")
	LeftCurly  = StandardToken("{")
	RightCurly = StandardToken("}")
	Comma      = StandardToken(",")
	Semicolon  = StandardToken(";")
	Comment    = StandardToken("#")
	Newline    = StandardToken("\n")
	If         = StandardToken("if")
	Else       = StandardToken("else")
	Def        = StandardToken("def")
	Extern     = StandardToken("extern")
	Return     = StandardToken("return")
	Plus       = BinaryOpToken("+")
	Minus      = BinaryOpToken("-")
	Times      = BinaryOpToken("*")
	Divide     = BinaryOpToken("/")
	Mod        = BinaryOpToken("%")
	Equals     = BinaryOpToken("==")
	Assign     = BinaryOpToken("=")
)

// StaticTokenMap maps source code strings to corresponding StaticTokens.
var StaticTokenMap = make(map[string]StaticToken)

func init() {
	for _, tok := range [...]StandardToken{
		LeftParen,
		RightParen,
		LeftCurly,
		RightCurly,
		Comma,
		Semicolon,
		Comment,
		Newline,
		If,
		Else,
		Def,
		Extern,
		Return,
	} {
		StaticTokenMap[string(tok)] = tok
	}
	for _, tok := range [...]BinaryOpToken{
		Plus,
		Minus,
		Times,
		Divide,
		Mod,
		Equals,
		Assign,
	} {
		StaticTokenMap[string(tok)] = tok
	}
}

// Implement Stringer for Tokens
func (t StandardToken) String() string {
	if t == Newline {
		return "token(\\n)"
	}
	return "token(" + string(t) + ")"
}
func (t BinaryOpToken) String() string {
	return "token(" + string(t) + ")"
}
func (t Identifier) String() string {
	return "token(" + string(t) + ")"
}
func (t Number) String() string {
	return fmt.Sprintf("token(%f)", t)
}
