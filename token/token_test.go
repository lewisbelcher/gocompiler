package token

import "testing"

// NB: these tests are primarily for convenience just to check for
// the soundness of token.go

func TestTokensInSwitch(t *testing.T) {
	tok := Token(Identifier("hello"))
	switch tok := tok.(type) {
	case StaticToken:
		t.Errorf("tok %v gave the wrong return type", tok)
	case Number:
		t.Errorf("tok %v gave the wrong return type", tok)
	}

	tok = Token(Number(4.5))
	switch tok := tok.(type) {
	case StaticToken:
		t.Errorf("tok %v gave the wrong return type", tok)
	case Identifier:
		t.Errorf("tok %v gave the wrong return type", tok)
	}

	tok = Token(Plus)
	switch tok := tok.(type) {
	case Number:
		t.Errorf("tok %v gave the wrong return type", tok)
	case Identifier:
		t.Errorf("tok %v gave the wrong return type", tok)
	}
}
