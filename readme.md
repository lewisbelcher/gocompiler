Compiler for Kaleidoscope Written in Go
=======================================

Following the tutorial laid out [here](https://harlanhaskins.com/2017/01/08/building-a-compiler-with-swift-in-llvm-part-1-introduction-and-the-lexer.html).

Notes
-----

Kaleidoscope only has double types, so we can get away with not specifying
types in declarations / returns.

Kaleidoscope should also be able to import names from the C standard library.
For example: to import the `math.h` function `double sqrt(double n);` one
should type:
```
extern sqrt(n);
```

### LLVM

To install LLVM it was much faster to fetch the source directly from
`LLVM.org`: `git clone http://LLVM.org/git/LLVM.git` (clone into an appropriate
`GOPATH` location then follow the readme for install instructions).

When building programs using the Go bindings, the size of the binaries were
**huge** - 1.2GB huge!..  (even the simplest of programs produce large binaries
and this is [a known 'feature' of Go](https://github.com/golang/go/issues/6853)).
To combat this it becomes very useful to strip the binary of its symbol table
when building (`-ldflags="-s -w"`), although this reduces debugability, it also
reduces binary sizes to a more 'reasonable' 115MB.

Note that we'll also need to install the LLVM toolchain. On Arch this is simply
```
pacman -S LLVM
```

### Creating an executable

To create an executable, we _could_ use the built in execution engine and the
Go runtime. But to create a standalone (dynamically linked) executable, we
have to use the LLVM compiler `llc` to create an object/assembly file, then
use a native linker:
```
llc -filetype={obj,asm} -relocation-model=pic <file>.{bc,ll}
gcc [-lm] -o <file> <file>.{s,o}  # NB use -lm to specifically link libm if required
./<file>
```
LLVM does have its own linker (`lld`), but using it to create executables from
the bindings will require some research...
